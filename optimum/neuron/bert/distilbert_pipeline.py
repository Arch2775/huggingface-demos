from optimum.neuron.pipelines import pipeline

# Compile model
# model_id ="juliensimon/distilbert-amazon-shoe-reviews"
input_shapes = {"batch_size": 1, "sequence_length": 128}
# export = True

# Load compiled model
model_id ="./distilbert_neuron"
export = False

sample = "This is a really nice pair of shoes, I like them a lot."

pipe = pipeline(task="text-classification", model=model_id, input_shapes=input_shapes, export=export)
response = pipe(sample)
print(response)
