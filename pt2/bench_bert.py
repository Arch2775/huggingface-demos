import torch
from transformers import BertTokenizer, BertModel
import intel_extension_for_pytorch as ipex
import time

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
orig_model = BertModel.from_pretrained("bert-base-uncased").to(device=device)
orig_model.eval()
text = "Replace me by any text you'd like. " * 12 
print(f"Sequence length: {len(text)}")
encoded_input = tokenizer(text, return_tensors='pt').to(device=device)

def bench(model, input, n=1000):
    with torch.no_grad():
        # Warmup
        model(**encoded_input)
        start = time.time()
        for _ in range(n):
            model(**input)
        end = time.time()
        return (end - start) * 1000 / n

print(f"Average time: {bench(orig_model, encoded_input):.2f} ms")

print(torch._dynamo.list_backends())
model = torch.compile(orig_model, backend="inductor")
print(f"Average time inductor: {bench(model, encoded_input):.2f} ms")

torch._dynamo.reset()
model = ipex.optimize(orig_model)
model = torch.compile(model, backend="ipex")
print(f"Average time ipex: {bench(model, encoded_input):.2f} ms")




